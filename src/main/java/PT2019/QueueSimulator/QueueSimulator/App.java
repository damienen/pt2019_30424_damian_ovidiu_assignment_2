package PT2019.QueueSimulator.QueueSimulator;

import javax.swing.*;
import userInterface.UI;
public class App 
{
    public static void main( String[] args )
    {
    	SwingUtilities.invokeLater(new Runnable() {
    		public void run() {
    			new UI();
    		}
    	});
	
    }
}

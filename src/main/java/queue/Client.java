package queue;
import java.util.*;
public class Client implements Cloneable{
	private int cID;
	private int serveTime;
	private int servedFor;
	private int waitTime;
	public Client(int cID,int serveTime) {
		this.cID=cID;
		this.serveTime=serveTime;
		this.waitTime=0;
		this.servedFor=0;
	}
	public int getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}
	public void incrementWaitTime() {
		this.waitTime++;
	}
	public int getcID() {
		return cID;
	}
	public int getServeTime() {
		return serveTime;
	}
	public int getServedFor() {
		return servedFor;
	}
	public void setServedFor(int servedFor) {
		this.servedFor = servedFor;
	}
	public void incrementServedFor() {
		this.servedFor++;
	}
	public String toString() {
		return Character.toString((char)0x25CF);
	}
	public Object clone()throws CloneNotSupportedException{  
		return super.clone();  
		}  
}

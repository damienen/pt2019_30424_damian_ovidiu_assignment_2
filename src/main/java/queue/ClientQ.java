package queue;
import java.util.*;
import javax.swing.*;
public class ClientQ extends Thread{
	private Vector<Client> clients=new Vector();
	private Vector<Client> servedClients=new Vector();
	private int qID;
	private int emptyTime;
	private int elapsedTime;
	private int simulationTime;
	private int peakTime;
	private int maxClients;
	private JLabel Qll;
	private JTextArea stat;
	private JTextArea log;
	public ClientQ(int qID,int simulationTime,JLabel Qll,JTextArea stats,JTextArea logs) {
		this.qID=qID;
		this.emptyTime=0;
		this.elapsedTime=0;
		this.simulationTime=simulationTime;
		this.Qll=Qll;
		this.stat=stats;
		this.log=logs;
		this.peakTime=0;
		this.maxClients=0;
	}
	public synchronized int getqID() {
		return this.qID;
	}
	public int getPeakTime() {
		return peakTime;
	}
	public synchronized String addClient(Client c) {
		clients.addElement(c);
		String result="Client "+c.getcID()+" came to queue "+this.qID;
		if(clients.size()>maxClients) {
			maxClients=clients.size();
			peakTime=elapsedTime;}
		return result;
	}
	public synchronized void toStat(String x) {
		stat.setText(stat.getText()+x);
	}
	public synchronized void toLog(String x) {
		log.setText(log.getText()+x);
	}
	public synchronized String serveClient() throws InterruptedException, CloneNotSupportedException {
		String result="";
		if(clients.size()==0) {
			emptyTime++;
			//result+="\n"+elapsedTime+":Queue "+this.qID+" is empty.";
			}
		//while(clients.size()==0)
			//wait();
		else {
		Client c=(Client)clients.elementAt(0);
		if(c.getServedFor()==c.getServeTime()) {
			servedClients.addElement((Client) c.clone());
			clients.removeElementAt(0);
			toLog(elapsedTime+":Client served on queue "+this.qID+"\n");
		}else {
			c.incrementServedFor();
		}
		for(Client a:clients) {
			a.incrementWaitTime();
		}}
		//toLog(result);
		//System.out.println("\n"+qID+" "+this.toString());
		return result;
	}
	public synchronized String toString() {
		String result="";
		for(Client k:clients)
			result+=k.toString();
		result+=Character.toString((char)0x25A0);
		return result;
	}
	public synchronized float getAverageWaitingTime() {
		float n=0;
		float result=0;
		for(Client c:clients) {
			result+=c.getWaitTime();
			n++;
		}
		for(Client c:servedClients) {
			result+=c.getWaitTime();
			n++;
		}
		return result/n;
	}
	public synchronized float getAverageServiceTime() {
		float n=0;
		float result=0;
		for(Client c:clients) {
			result+=c.getServeTime();
			n++;
		}
		for(Client c:servedClients) {
			result+=c.getServeTime();
			n++;
		}
		return result/n;
	}
	public synchronized int getEmptyTime() {
		return emptyTime;
	}
	public synchronized int getClientsToServe() {
		return clients.size();
	}
	public synchronized int getClientsServed() {
		return servedClients.size();
	}
	private synchronized void QqtoS() {
		Qll.setText(clients.toString());
	}
	public void run(){
		while(elapsedTime<simulationTime) {
			elapsedTime++;
			try {
				this.serveClient();
				QqtoS();
			}catch(InterruptedException e){}catch(CloneNotSupportedException e) {}
			try {
				sleep(1000);
			}catch (InterruptedException e) {}
		}
	}
	}

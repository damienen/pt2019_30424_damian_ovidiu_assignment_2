package queue;
import java.util.*;

import javax.swing.JLabel;
import javax.swing.JTextArea;
public class Shop extends Thread {
	private ClientQ qs[];
	private int nrOfQs;
	private int sID;
	private int elapsedTime;
	private int simulationTime;
	private int minArrival;
	private int maxArrival;
	private int minServe;
	private int maxServe;
	private Vector<JLabel> Ql;
	private JTextArea stat;
	private JTextArea log;
	public Shop(int sID,int nrOfQs,int simulationTime,int minArrival,
			int maxArrival,int minServe,int maxServe,Vector<JLabel> Ql,JTextArea stats,JTextArea logs) {
		this.sID=sID;
		this.nrOfQs=nrOfQs;
		this.simulationTime=simulationTime;
		this.qs=new ClientQ[this.nrOfQs];
		this.elapsedTime=0;
		this.minArrival=minArrival;
		this.maxArrival=maxArrival;
		this.minServe=minServe;
		this.maxServe=maxServe;
		this.stat=stats;
		this.log=logs;
		for(int i=0;i<nrOfQs;i++) {
			qs[i]=new ClientQ(i,simulationTime,Ql.elementAt(i),stat,log);
		}
	}
	
	public ClientQ getQs(int i) {
		return qs[i];
	}
	private synchronized int minClientsIndex() {
		int index=0;
		//try{
		int min=qs[0].getClientsToServe();
			for(int i=1;i<this.nrOfQs;i++) {
				if(qs[i].getClientsToServe()<min) {
					min=qs[i].getClientsToServe();
					index=i;
				}
			}
			//}catch(InterruptedException e) {	}
			return index;
	}
	public synchronized void QtoLS(int i) {
		Ql.elementAt(i).setText(qs[i].toString());
	}
	public synchronized void toStat() {
		String s="";
		for(int i=0;i<nrOfQs;i++) {
			s+="Queue "+i+":\n";
			s+="Average waiting time:"+qs[i].getAverageWaitingTime()+"\n";
			s+="Average serving time:"+qs[i].getAverageServiceTime()+"\n";
			s+="Empty time:"+qs[i].getEmptyTime()+"\n";
			s+="Peak time:"+qs[i].getPeakTime()+"\n";}
		stat.setText(stat.getText()+s);
	}
	public synchronized void toLog(String x) {
		log.setText(log.getText()+x);
	}
	public void run() {
		try {
			int cID=0;
			Random r=new Random();
			int rv=r.nextInt(maxArrival-minArrival)+minArrival+elapsedTime;
			while(elapsedTime<simulationTime) {
				elapsedTime++;
				if(rv==elapsedTime) {
					int m=minClientsIndex();
					qs[m].addClient(new Client(cID,r.nextInt(maxServe-minServe)+minServe));
					rv=r.nextInt(maxArrival-minArrival)+minArrival+elapsedTime;
					log.setText(log.getText()+elapsedTime+":Client added to queue "+m+"\n");
				}
				sleep(1000);
			}
				toStat();
				}catch(InterruptedException e) {	}
		}
	}

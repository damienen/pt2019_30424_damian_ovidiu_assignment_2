package userInterface;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.Vector;

import javax.swing.*;

import queue.*;

public class UI {
	private Vector<JLabel> Ql=new Vector<JLabel>();
	private JTextArea stats=new JTextArea(10,15);
	private JTextArea logs=new JTextArea(10,15);
	private JSpinner minArrive=new JSpinner(new SpinnerNumberModel(5,1,14,1));
	private JSpinner maxArrive=new JSpinner(new SpinnerNumberModel(10,1,15,1));
	private JSpinner minServe=new JSpinner(new SpinnerNumberModel(5,1,14,1));
	private JSpinner maxServe=new JSpinner(new SpinnerNumberModel(10,1,15,1));
	private JButton sim=new JButton("Simulate");
	private JSpinner maxSim=new JSpinner(new SpinnerNumberModel(100,1,200,1));
	private class simActionListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			stats.setText("");
			logs.setText("");
			try {maxSim.commitEdit();} catch ( java.text.ParseException ee ) { }int simulationTime = (Integer) maxSim.getValue();
			try {minArrive.commitEdit();} catch ( java.text.ParseException ee ) { }int miniArrive = (Integer) minArrive.getValue();
			try {maxArrive.commitEdit();} catch ( java.text.ParseException ee ) { }int maxiArrive = (Integer) maxArrive.getValue();if(miniArrive==maxiArrive)maxiArrive++;
			try {minServe.commitEdit();} catch ( java.text.ParseException ee ) { }int miniServe = (Integer) minServe.getValue();
			try {maxServe.commitEdit();} catch ( java.text.ParseException ee ) { }int maxiServe = (Integer) maxServe.getValue();if(miniServe==maxiServe)maxiServe++;
			Shop s=new Shop(0,3,simulationTime,miniArrive,maxiArrive,miniServe,maxiServe,Ql,stats,logs);
			s.start();
			for(int i=0;i<3;i++) {
				s.getQs(i).start();
			}
		}
		
	}
public UI() {
	Ql.addElement(new JLabel("Queue 0"));
	Ql.addElement(new JLabel("Queue 1"));
	Ql.addElement(new JLabel("Queue 2"));
	JScrollPane scrollLog=new JScrollPane(logs);
	JScrollPane scrollStats=new JScrollPane(stats);
	JFrame frame=new JFrame();
	JPanel panel=new JPanel(new GridBagLayout());
	frame.getContentPane().add(panel);
	GridBagConstraints c=new GridBagConstraints();
	stats.setEditable(false);
	logs.setEditable(false);
	c.weightx=0;
	c.gridx=0;c.gridy=1; panel.add(Ql.elementAt(0),c);
	c.gridy=3;panel.add(Ql.elementAt(1),c);
	c.gridy=5;panel.add(Ql.elementAt(2),c);
	c.anchor=GridBagConstraints.CENTER;
	c.gridx=1;c.gridy=0;panel.add(new JLabel("Stats:"),c);
	c.gridx=7;c.gridy=0;panel.add(new JLabel("Logs:"),c);
	c.gridx=3;c.gridy=1;panel.add(new JLabel("Arriving time between customers:"),c);
	c.gridx=3;c.gridy=2;panel.add(new JLabel("Serving time:"),c);
	c.gridx=3;c.gridy=3;panel.add(new JLabel("Simulate for:"),c);
	c.gridx=1;c.gridy=1;c.gridheight=6;panel.add(scrollStats,c);
	c.gridx=7;c.gridy=1;panel.add(scrollLog,c);
	c.gridx=4;c.gridy=1;c.gridheight=1;panel.add(minArrive,c);
	c.gridx=5;c.gridy=1;panel.add(new JLabel(" to "),c);
	c.gridx=6;c.gridy=1;panel.add(maxArrive,c);
	c.gridx=4;c.gridy=2;panel.add(minServe,c);
	c.gridx=5;c.gridy=2;panel.add(new JLabel(" to "),c);
	c.gridx=6;c.gridy=2;panel.add(maxServe,c);
	c.gridx=4;c.gridy=3;c.gridwidth=3;panel.add(maxSim,c);
	//c.gridx=5;c.gridy=3;panel.add(new JLabel(" to "),c);
	//c.gridx=6;c.gridy=3;panel.add(maxSim,c);
	c.anchor=GridBagConstraints.CENTER;
	c.gridwidth=4;c.gridheight=1;
	c.gridx=3;c.gridy=4;panel.add(sim,c);
	frame.setPreferredSize(new Dimension(950,250));
	frame.setSize(frame.getPreferredSize());
	frame.setLocationRelativeTo(null);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setResizable(false);
	frame.setVisible(true);
	
	sim.addActionListener(new simActionListener());
}
}
